package helpers

case class SensorRange(min: Int, avg: Int, max: Int)
case class NaNSensorRange(min: String)
