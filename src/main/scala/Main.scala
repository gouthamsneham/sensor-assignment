import helpers.SensorRange

import java.io.{File, FileInputStream}
import scala.collection.mutable
import scala.io.{BufferedSource, Source}
object Main {
  def main(args: Array[String]): Unit = {
    val files: List[File] = new File(args.head).listFiles.filter(_.isFile).toList


    var filesProcessed: Int = 0
    var numberOfProcessedMeasurements: Long = 0L
    var numberOfFailedMeasurements: Long = 0L

    val SensorData: mutable.Map[String, Long] = mutable.Map.empty
    val validSensorEntries: mutable.Map[String, SensorRange] = mutable.Map.empty
    val invalidSensorEntries: mutable.Map[String, String] = mutable.Map.empty

    files.foreach { f =>
      filesProcessed += 1
      val fileInputStream: FileInputStream = new FileInputStream(f)
      val bufferedSource: BufferedSource = Source.createBufferedSource(fileInputStream, 1024)
      val linesFromBuffer: Iterator[String] = bufferedSource.getLines().drop(1)

      for (line <- linesFromBuffer) {
        val columns: Array[String] = line.split(",").map(_.trim)
        val Id: String = columns(0)

        if (!columns(1).contains("NaN")) {
          val sensorValue: Int = columns(1).toInt

          numberOfProcessedMeasurements += 1

          if (SensorData.contains(Id)) {
            val min: Int = validSensorEntries(Id).min
            val max: Int = validSensorEntries(Id).max

            SensorData(Id) += 1

            if (sensorValue < min) validSensorEntries(Id) = SensorRange(sensorValue, (sensorValue + max) / 2, max)
            else if (sensorValue > max) validSensorEntries(Id) = SensorRange(min, (min + sensorValue) / 2, sensorValue)
            else validSensorEntries(Id) = SensorRange(min, (min + max) / 2, max)
          } else {
            SensorData addOne(Id, 1L)
            validSensorEntries addOne(Id, SensorRange(sensorValue, sensorValue, sensorValue))
          }
        } else {
          if (!validSensorEntries.contains(Id)) {
            invalidSensorEntries addOne(Id, "NaN")
          }
          numberOfFailedMeasurements += 1
        }
      }

      bufferedSource.close()
    }

    println(s"Num of processed files: $filesProcessed")
    println(s"Num of processed measurements: $numberOfProcessedMeasurements")
    println(s"Num of failed measurements: $numberOfFailedMeasurements")
    println("\nSensors with highest avg value:\n")
    println("sensor-id,min,avg,max")
    println("---------------------")

    validSensorEntries.toList.sortWith((left, right) => left._2.avg > right._2.avg)
      .foreach(sre => println(sre._1 + "," + sre._2.min + "," + sre._2.avg + "," + sre._2.max))
    invalidSensorEntries.foreach(sre => println(sre._1 + "," + sre._2 + "," + sre._2 + "," + sre._2))
  }
}
