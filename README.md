# sensor-assignment



## Pre-requisites
 
 Java
 SBT
 Scala

 Clone the repo 
 ```
 git clone https://gitlab.com/gouthamsneham/sensor-assignment.git

 ```
 Change directoryto the cloned repo

```
cd sensor-assignment

```
Package the code to a runnable artid=fact /jar

```
sbt assembly
```

Run the Artifact by passing  of csv files directory path as argument


```
java -jar sensor-assignment/target/scala-2.13/sensor-assignment-assembly-0.1.0-SNAPSHOT.jar <csv file directory path>


```
